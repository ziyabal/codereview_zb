//
//  DetailViewController.m
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.labelArtistName.text = self.artist.artistName;
    self.labelTrackName.text = self.artist.trackName;
    self.labelAlbumName.text = self.artist.albumName;
    self.labelPrice.text = self.artist.price;
    self.labelReleaseDate.text = self.artist.releaseDate;
    
    // Load the image async
    // We want not to wait when creating the tableview with all the result information
    Artist *artistAsyncImage = [[Artist alloc] init];
    [artistAsyncImage loadAsyncImageDataWithURL:[NSURL URLWithString:self.artist.artwork]
                             andSuccessCallback:^(NSData *imageData) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     self.imageArtwork.image = [UIImage imageWithData:imageData];
                                 });
                             }];
    
    // Make UIImageView shown round to the user.
    self.imageArtwork.layer.cornerRadius = self.imageArtwork.frame.size.width / 2;
    self.imageArtwork.clipsToBounds = YES;
    
    self.imageArtwork.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imageArtwork.layer.borderWidth = 4.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
