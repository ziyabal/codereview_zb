//
//  Artist.h
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Artist : NSObject

@property (nonatomic, copy) NSString *artistName;
@property (nonatomic, copy) NSString *trackName;
@property (nonatomic, copy) NSString *albumName;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *releaseDate;
@property (nonatomic, copy) NSString *artwork;

+ (Artist *)initArtistWithArtistName:(NSString *)artistName andTrackName:(NSString *)trackName andAlbumName:(NSString *)albumName andPrice:(NSString *)price andReleaseDate:(NSString *)releaseDate andArtwork:(NSString *)artwork;

- (void)loadAsyncImageDataWithURL:(NSURL *)imageURL andSuccessCallback:(void(^)(NSData *imageData))onSuccessCallback;

@end
