//
//  DetailViewController.h
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Artist.h"

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelArtistName;
@property (weak, nonatomic) IBOutlet UILabel *labelTrackName;
@property (weak, nonatomic) IBOutlet UILabel *labelAlbumName;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelReleaseDate;
@property (weak, nonatomic) IBOutlet UIImageView *imageArtwork;

@property (strong, nonatomic) Artist *artist;

@end
