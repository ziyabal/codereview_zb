//
//  ArtistTableViewCell.h
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelArtistName;
@property (weak, nonatomic) IBOutlet UILabel *labelTrackName;
@property (weak, nonatomic) IBOutlet UIImageView *imageArtwork;

@end
