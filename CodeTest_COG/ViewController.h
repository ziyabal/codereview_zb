//
//  ViewController.h
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "iTunesSearchApi.h"

@interface ViewController : UIViewController <iTunesSearchDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSArray *datasourceSearchResults;

@end

