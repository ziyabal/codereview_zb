//
//  Artist.m
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import "Artist.h"

@implementation Artist

+ (Artist *)initArtistWithArtistName:(NSString *)artistName andTrackName:(NSString *)trackName andAlbumName:(NSString *)albumName andPrice:(NSString *)price andReleaseDate:(NSString *)releaseDate andArtwork:(NSString *)artwork
{
    Artist *artist = [[Artist alloc] init];
    
    artist.artistName = artistName;
    artist.trackName = trackName;
    artist.albumName = albumName;
    artist.price = price;
    artist.releaseDate = releaseDate;
    artist.artwork = artwork;
    
    return artist;
}

- (void)loadAsyncImageDataWithURL:(NSURL *)imageURL andSuccessCallback:(void(^)(NSData *imageData))onSuccessCallback
{
    // Async task to get the image from server.
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:imageURL completionHandler:^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
        if (data) {
            onSuccessCallback(data);
        }
    }];
    
    [task resume];
}

@end
