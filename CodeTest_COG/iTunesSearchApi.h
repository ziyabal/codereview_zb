//
//  iTunesSearchApi.h
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol iTunesSearchDelegate;


@interface iTunesSearchApi : NSObject

@property (nonatomic, assign) id<iTunesSearchDelegate> delegate;

- (void)performiTunesSearchWithBaseURL:(NSString *)baseURL andSearchTerm:(NSString *)searchTerm;

@end


@protocol iTunesSearchDelegate <NSObject>

- (void)resultDidCompleteWithArray:(NSArray *)resultsArrray andError:(NSError *)error;

@end
