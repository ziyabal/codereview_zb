//
//  iTunesSearchApi.m
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import "iTunesSearchApi.h"
#import "Artist.h"
#import "Constants.h"

@implementation iTunesSearchApi

- (void)performiTunesSearchWithBaseURL:(NSString *)baseURL andSearchTerm:(NSString *)searchTerm
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseURL, searchTerm];
 
    // Prepare the connection
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];

    // Send an async request and async wait for the handler.
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                        NSError *connectionError) {
                               
                               // Use temp dictionary and array to get the useable 'root' of the results.
                               NSDictionary *tempResultsFeedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                               NSArray *tempResultsFeedArray = [tempResultsFeedDictionary objectForKey:@"results"];
                               
                               // Do a loop to collect the information and make it an Artist object.
                               // Add the Artist object to the mutablearray
                               NSMutableArray *resultsMutableArray = [[NSMutableArray alloc] init];
                               for (int i = 0; i < tempResultsFeedArray.count; i++) {
                                   NSDictionary *dictArtist = [tempResultsFeedArray objectAtIndex:i];
                                   
                                   NSString *artistName = [dictArtist objectForKey:kArtistName];
                                   NSString *trackName = [dictArtist objectForKey:kTrackName];
                                   NSString *albumName = [dictArtist objectForKey:kAlbumName];
                                   NSString *price = [NSString stringWithFormat:@"$%.2f", [[dictArtist objectForKey:kPrice] floatValue]];
                                   NSString *releaseDate = [dictArtist objectForKey:kReleaseDate];
                                   NSString *artwork = [NSString stringWithFormat:@"%@", [dictArtist objectForKey:kArtwork]];
                          
                                   Artist *artist = [Artist initArtistWithArtistName:artistName andTrackName:trackName andAlbumName:albumName andPrice:price andReleaseDate:releaseDate andArtwork:artwork];
                                   [resultsMutableArray addObject:artist];
                               }
                               
                               // Delegate method to inform 'ViewController' to pass the array.
                               NSArray *convertedResultsArray = [resultsMutableArray mutableCopy];
                               if (self.delegate) {
                                   [self.delegate resultDidCompleteWithArray:convertedResultsArray andError:connectionError];
                               }
                           }];
}

@end
