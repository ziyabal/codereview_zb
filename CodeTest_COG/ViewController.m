//
//  ViewController.m
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

#import "ViewController.h"
#import "ArtistTableViewCell.h"
#import "DetailViewController.h"
#import "Artist.h"
#import "Constants.h"

@interface ViewController ()

@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

#pragma mark - UITableView Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // If there is no result to show, provide one cell for feedback for the user.
    if (self.datasourceSearchResults.count == 0 || self.datasourceSearchResults == nil) {
        return 1;
    }
    return self.datasourceSearchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArtistTableViewCell *cell = (ArtistTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ArtistTableViewCell"];
    // Reset some values to original if there was a "No result" cell was shown in the past of use.
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.labelTrackName.hidden = NO;
    cell.imageArtwork.hidden = NO;
    
    if (self.datasourceSearchResults.count > 0) {
        // Pass value from the array to 'Artist' object
        Artist *artist = [self.datasourceSearchResults objectAtIndex:indexPath.row];
        cell.labelArtistName.text = artist.artistName;
        cell.labelTrackName.text = artist.trackName;
        
        // Load the image async
        // We want not to wait when creating the tableview with all the result information
        Artist *artistAsyncImage = [[Artist alloc] init];
        [artistAsyncImage loadAsyncImageDataWithURL:[NSURL URLWithString:artist.artwork]
                                 andSuccessCallback:^(NSData *imageData) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         cell.imageArtwork.image = [UIImage imageWithData:imageData];
                                     });
                                 }];
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;

        cell.labelArtistName.text = @"No results";
        
        cell.labelTrackName.hidden = YES;
        cell.imageArtwork.hidden = YES;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Storyboard Helper Method
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DetailViewController *viewController = [segue destinationViewController];
    // Pass some values to the destination view 'DetailView'
    Artist *artist = [self.datasourceSearchResults objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    viewController.artist = artist;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"pushDetail"]) {
        // Check if datasource has no object, because we don't want to push it to detailview
        if (self.datasourceSearchResults.count == 0) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - SearchBar Method
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelSearchAction];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    self.searchBar.text  = @"";
    [self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text length] > 0) {
        // Add the spinner programmaticaly to show the user feedback when busy getting all the information.
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.spinner.center = self.view.center;
        [self.view addSubview:self.spinner];
        [self.spinner startAnimating];
        
        // Add '+' char to replace it for the whitespaces in provided text in searchbar.
        NSString *searchText = [searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@"+"];

        // Give the values to the 'iTunesSearchApi'
        // BaseURL
        // User provided formatted text
        iTunesSearchApi *searchApi = [[iTunesSearchApi alloc] init];
        searchApi.delegate = self;
        [searchApi performiTunesSearchWithBaseURL:kiTunesSearchBaseURL andSearchTerm:searchText];
    }
    
    [self.searchBar setShowsCancelButton:NO animated:YES];
}

- (void)cancelSearchAction
{
    [self.searchBar resignFirstResponder];
    self.searchBar.text  = @"";
}

#pragma mark - iTunesSearchApiDelegate Methods
- (void)resultDidCompleteWithArray:(NSArray *)resultsArrray andError:(NSError *)error
{
    // When finished retreiving information from backend, remove the spinner.
    [self.spinner removeFromSuperview];
    self.spinner = nil;
    
    // Collect the results and reload the tableview.
    self.datasourceSearchResults = resultsArrray;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

@end
