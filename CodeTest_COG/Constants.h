//
//  Constants.h
//  CodeTest_COG
//
//  Created by Ziya Bal on 15-01-16.
//  Copyright © 2016 Ziya Bal. All rights reserved.
//

// Constant for BaseURL
#define kiTunesSearchBaseURL @"http://itunes.apple.com/search?term="

// Constant for Artist object
#define kArtistName @"artistName"
#define kTrackName @"trackName"
#define kAlbumName @"collectionName"
#define kPrice @"trackPrice"
#define kReleaseDate @"releaseDate"
#define kArtwork @"artworkUrl100"
