A simple iOS mobile application to async a connection to a iTunes endpoint. The main goal is to retrieve Artist/Album results for a given search term and show it to the user.

The user provides a search term and then the application retrieves information from the endpoint. The results are displayed in a UITableView with a custom UITableViewCell. An async call is made to retrieve the image that is used for the album and/or artist.

A detail view is included to show more information about the item that is selected in the 'RootController'. The image in the detail view is displayed as a perfect round image.